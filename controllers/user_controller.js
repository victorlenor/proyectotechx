//variables de entorno
var baseMLabURL="https://api.mlab.com/api/1/databases/techu3db/collections/";
//var apiKeyMLab="apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF";
var express = require('express');
// var app =  express();
const cors = require('cors');
const apiKeyMLab="apiKey="+process.env.MLAB_API_KEY;
let requestJSON = require('request-json');
var queryStringField = 'f={"_id":0}&';

// app.use(cors());
// app.options('*',cors());

//peticion GET ALL de todos los 'users' CON MLAB
    function getUsers(req,res){
          var httpClient = requestJSON.createClient(baseMLabURL);
          httpClient.get('user?'+queryStringField+apiKeyMLab,
          function(err, respuestaMLab, body)
          {
            var response = {};
            if(err){
              response = {"msg":"error obteniendo usuario."};
              res.status(500);
            }
            else {
                if (body.length>0) {
                  response = body;
                }
                else {
                  response = {"msg":"ningun elemento usuario."};
                  res.status(404);
                }
            }
            res.send(response);
          });
      }

//peticion GET+ID de todos los 'users' CON MLAB
      function getUsersId(req,res){
              var id=req.params.id;
              var queryString = 'q={"idUsuario":'+id+'}&';
              var httpClient = requestJSON.createClient(baseMLabURL);
              httpClient.get('user?'+queryString+queryStringField+apiKeyMLab,
              function(err, respuestaMLab, body)
              {
                  var response = {};
                if(err){
                  response = {"msg":"error obteniendo usuario."};
                  res.status(500);
                }
                else {
                    if (body.length>0) {
                      response = body;
                      }
                    else {
                      response = {"msg":"ningun elemento usuario."};
                      res.status(404);
                    }
                }
                res.send(response)
                });
        };

//Petición mostrar todas las cuentas asociadas a un usuario ID
  function getAccountUserId(req,res){
            var id=req.params.id;
            var queryStringID = 'q={"userID":'+id+'}&';
            var httpClient = requestJSON.createClient(baseMLabURL);
            httpClient.get('account?'+queryStringID+queryStringField+apiKeyMLab,
            function(err, respuestaMLab, body)
            {
              var response = {};
              if(err){
                response = {"msg":"error obteniendo cuentas de usuario."};
                res.status(500);
              }
              else {
                  if (body.length>0) {
                    response = body;
                  }
                  else {
                    response = {"msg":"ninguna cuenta asociada al usuario."};
                    res.status(404);
                  }
              }
              res.send(response);
            });
  };

//POST con MLAB: insertar un usuario
 function postInsertUser(req, res){
         var clienteMlab = requestJSON.createClient(baseMLabURL);
         clienteMlab.get('user?' + apiKeyMLab,
           function(error, respuestaMLab, body){
             newID = body.length + 1;
             var newUser = {
                "idUsuario" : newID,
               "first_name" : req.body.first_name,
                "last_name" : req.body.last_name,
                    "email" : req.body.email,
                 "password" : req.body.password
             };
             clienteMlab.post(baseMLabURL + "user?" + apiKeyMLab, newUser,
               function(error, respuestaMLab, body){
                 res.status(201);
                 res.send(body);
               });
           });
};

//PUT actualizar USERS con parámetro 'id' MLAB
function putUpdateUserId(req, res) {
    var id = req.params.id;
    var queryStringID = 'q={"idUsuario":' + id + '}&';
    var clienteMlab = requestJSON.createClient(baseMLabURL);
    clienteMlab.get('user?'+ queryStringID + apiKeyMLab,
    function(error, respuestaMLab, body) {
      var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
        clienteMlab.put(baseMLabURL +'user?' + queryStringID + apiKeyMLab, JSON.parse(cambio),
        function(error, respuestaMLab, body) {
          res.send(body);
        });
    });
};
//PUT 2 CON MLAB
function put2UpdateUserId(req, res) {
     var id = req.params.id;
     let userBody = req.body;
     var queryString = 'q={"id":' + id + '}&';
     var httpClient = requestJSON.createClient(baseMLabURL);
     httpClient.get('user?' + queryString + apiKeyMLab,
       function(err, respuestaMLab, body){
         let response = body[0];
         //Actualizo campos del usuario
         let updatedUser = {
           "id" : req.body.id,
           "first_name" : req.body.first_name,
           "last_name" : req.body.last_name,
           "email" : req.body.email,
           "password" : req.body.password
         };
         httpClient.put('user/' + response._id.$oid + '?' + apiKeyMLab, updatedUser,
           function(err, respuestaMLab, body){
             var response = {};
             if(err) {
                 response = {
                   "msg" : "Error actualizando usuario."
                 }
                 res.status(500);
             } else {
               if(body.length > 0) {
                 response = body;
               } else {
                 response = {
                   "msg" : "Usuario actualizado correctamente."
                 }
                 res.status(200);
               }
             }
             res.send(response);
           });
       });
 };

//DELETE user with id
 function deleteUserId(req, res){
   var id = req.params.id;
   var queryStringID = 'q={"idUsuario":' + id + '}&';
   var httpClient = requestJSON.createClient(baseMLabURL);
   httpClient.get('user?' +  queryStringID + apiKeyMLab,
     function(error, respuestaMLab, body){
       var respuesta = body[0];
       httpClient.delete(baseMLabURL + "user/" + respuesta._id.$oid +'?'+ apiKeyMLab,
         function(error, respuestaMLab,body){
           res.send(body);
       });
     });
 };

//Method POST login
function postLogin(req, res){

   let email = req.body.email;
   let pass = req.body.password;
   let queryString = 'q={"email":"' + email + '","password":"' + pass + '"}&';
   let limFilter = 'l=1&';
   let clienteMlab = requestJSON.createClient(baseMLabURL);

   clienteMlab.get('user?'+ queryString + limFilter + apiKeyMLab,
     function(error, respuestaMLab, body) {
       if(!error) {
         if (body.length == 1) {
           let login = '{"$set":{"logged":true}}';
           clienteMlab.put('user?q={"idUsuario": ' + body[0].idUsuario + '}&' + apiKeyMLab, JSON.parse(login),
             function(errPut, resPut, bodyPut) {
               res.send({'msg':'Login correcto', 'user':body[0].email, 'idUsuario':body[0].idUsuario});
             });
         }
         else {
           res.status(404).send({"msg":"Usuario no válido."});
         }
       } else {
         res.status(500).send({"msg": "Error en petición a mLab."});
       }
   });
};

//Method POST loggout
 function postLoggout(req, res){
   let email = req.body.email;
   let queryString = 'q={"email":"' + email + '","logged":true}&';
   let limFilter = 'l=1&';
   let clienteMlab = requestJSON.createClient(baseMLabURL);

   clienteMlab.get('user?'+ queryString + limFilter + apiKeyMLab,
     function(error, respuestaMLab, body) {
       if(!error) {
         if (body.length == 1) { // Existe un usuario que cumple 'queryString'
           let logout = '{"$unset":{"logged":true}}';
           clienteMlab.put('user?q={"idUsuario": ' + body[0].idUsuario + '}&' + apiKeyMLab, JSON.parse(logout),
           function(errPut, resPut, bodyPut) {
               res.send({'msg':'Loggout correcto', 'user':body[0].email, 'idUsuario':body[0].idUsuario});
               // If bodyPut.n == 1, put de mLab correcto
             });
         }
         else {
           res.status(404).send({"msg":"Usuario no válido."});
         }
       } else {
         res.status(500).send({"msg": "Error en petición a mLab."});
       }
   });
};

module.exports.getUsers = getUsers;
module.exports.getUsersId = getUsersId;
module.exports.getAccountUserId = getAccountUserId;
module.exports.postInsertUser = postInsertUser;
module.exports.putUpdateUserId = putUpdateUserId;
module.exports.put2UpdateUserId = put2UpdateUserId;
module.exports.deleteUserId = deleteUserId;
module.exports.postLogin = postLogin;
module.exports.postLoggout = postLoggout;
