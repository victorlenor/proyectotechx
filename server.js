// autor: Victor Rodriguez
// fecha: 17/07/19
// elemento:server.js - Practitioner

//variables de entorno
var express = require('express'); /*Variable que apunta al modulo express*/
var app =  express(); /*contiene todo lo que devuelva express*/
var bodyParser = require('body-parser');//para que se reconozca el jason enviado desde el cuerpo

var userFile = require('./user.json');//Bliblioteca para acceder archivos y ficheros creando referencia al archivo
var totalUsers = 0;//variable global contador de usuarios
const URL_BASE = '/apitechu/v1/';
//variable de entorno
const PORT = process.env.PORT || 3000;
//conexión a MLAB
var baseMLabURL="https://api.mlab.com/api/1/databases/techu3db/collections/";
//var baseMLabURL="https://api.mlab.com/api/1/databases/apitechuelb5ed/collections/"
const apiKeyMLab="apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF";
let requestJSON = require('request-json');
var queryStringField = 'f={"_id":0}&';
//peticiones asincronas callback (funcion anonima como parametro de otra funcion)
//peticion get de todos los 'users'

//agregamos cors sólo para pruebas en desarrollo
const cors = require('cors');
require('dotenv').config();//para usar variables de entorno
//
const user_controller = require('./controllers/user_controller');
//
app.use(cors());
app.options('*',cors());

app.use(bodyParser.json());//impotar librería esta instalado por defecto
//

app.get(URL_BASE + 'users',
    /*uri , COLLBAK FUNCION ANONIMA COMO PARAMETRO DE OTRA FUNCION (request y response)*/
    function(request,response)
    {
      console.log('GET',URL_BASE+'users');
      response.send(userFile);
    }
  );
//Peticion GET de un 'user' (Instance)
//se antepone : para indicar un parámetro
app.get(URL_BASE+'users/:id',
function(request,response)
  {
    console.log('GET' +URL_BASE+'users/id');
    //  console.log(request.params.id);
    // let para variable local, busqueda por indice
    let indice = request.params.id;
    let instancia = userFile[indice-1];
    console.log(instancia);
    let respuesta = (instancia !=undefined)? instancia : {"mensaje":"Recurso no encontrado"};
    //error http
    response.status(200);
    response.send(respuesta);

  })

//peticion GET con query string (ver en consola)
//http://localhost:3000/apitechu/v1/usersq?valor1=3&valor2="hola"
app.get(URL_BASE+'usersq',
function(request,response)
  {
    console.log('GET' +URL_BASE+' con query string');
    console.log(request.query);
    response.send(respuesta);
  })

// Petición POST enviando datos a través de BODY AÑADIR usuario
app.post(URL_BASE + 'users',
  function(req,res){
    totalUsers = userFile.length;
    var cuerpo = Object.keys(req.body).length;
    if (cuerpo > 0) {
      let newUser = {
        userId    : totalUsers+1,
        first_name : req.body.first_name,
        last_name  : req.body.last_name,
        email     : req.body.email,
        password  : req.body.password
      };
      userFile.push(newUser);
      res.status(200);//por buenas practicas enviar un codigo de aviso
      res.send({"mensaje":"Usuario creado con éxito","usuario": newUser});//para que no indique que se ha añadido conforme
    }
    else {
      res.send({"Error":"no se puede insertar vacío"});
    }
  })

// Petición PUT enviando datos a través de BODY MODIFICAR usuario
app.put(URL_BASE + 'users/:id',
   function(req,res){
     var cuerpo = Object.keys(req.body).length; //metodo para saber si el objeto recibido esta vacio
     if (cuerpo > 0) {
         let idp = req.params.id - 1;
         if(userFile[idp] != undefined){
           userFile[idp].first_name = req.body.first_name;
           userFile[idp].last_name = req.body.last_name;
           userFile[idp].email = req.body.email;
           userFile[idp].password = req.body.password;
           res.status(202);
           res.send({"mensaje" : "Usuario actualizado con exito.",
                     "Array" : userFile[idp]});
         }
         else{
           res.send({"mensaje":"Recurso no encontrado."})
         }
     }
     else {
       let varError = {"mensaje" : "No Existe parametros para Body"};
       res.send(varError)
     }
   })

// Petición DELETE enviando datos a través de BODY ELIMINAR usuario
   app.delete(URL_BASE + 'users/:id',
      function(req,res){
        let idu = req.params.id - 1;
        if(userFile[idu] != undefined){
          userFile.splice(idu, 1);
          res.status(204);
          res.send({"mensaje" : "Usuario eliminado con exito.",
                      "Array" : userFile[idu]});
            }
            else{
              res.send({"mensaje":"Recurso no encontrado."})
            }
      });

//LOGIN
app.post(URL_BASE + 'login',
  function(req,res){
    var user = req.body.email;
    var password = req.body.password;
    var estado;
    for(usu of userFile){
      if(usu.email == user){
        if(usu.password == password)
        {
          usu.logged = true;
          writeUserDataToFile(userFile);
          estado=true;
        }
        else{
          estado=false;
        }
      }
    }
    if(estado)
    {
      res.send ({"msg":"login correcto","idUsuario":usu.id,"logged":"true"});
    }
    else {
      {
        res.send({"msg":"login incorrecto"});
      }
    }
  }
);

//LOGOUT
//http://localhost:3000/apitechu/v1/logout
app.post(URL_BASE + 'logout',
  function(req,res){
    var user = req.body.email;
    for(usu of userFile){
      if(usu.email == user){
        var estado = String(usu.logged);
        if(estado == "true"){
          delete usu.logged;
          writeUserDataToFile(userFile);
          res.send ({"msg":"logout correcto","idUsuario":usu.id});
        }
        else {
          res.send ({"msg":"no loggeado","idUsuario":usu.id});
        }
    }
  }
}
);

//Funcion para escritura en el archivo user.json
function writeUserDataToFile(data) {
   var fs = require('fs');
   var jsonUserData = JSON.stringify(data);

   fs.writeFile("./user.json", jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
      if(err) {
        console.log(err);
      } else {
        console.log("Datos escritos en 'users.json'.");
      }
    })
 }

//Muestra solo los usuarios LOGGEADOS
//http://localhost:3000/apitechu/v1/loggeds
 app.get(URL_BASE + 'loggeds',
     function(req,res)
     {
       var usLogged = new Array();
       var y=0;
      for (usu of userFile)
      {
        if(usu.logged != undefined){
             usLogged[y] = usu;
             y++;
         }
       }
      let respuesta = (usLogged != undefined)? usLogged : {"msg":"no hay loggeados"};
      res.send (respuesta);
     }
   );

//Muestra USUARIOS en el RANGO indicado INI - FIN
//http://localhost:3000/apitechu/v1/loggedps?ini=7&fin=10
    app.get(URL_BASE + 'loggedps',
        function(req,res)
        {
          var ini = Number(req.query.ini);
          var fin = Number(req.query.fin);
          var x = 0;
          var usuRan = new Array();
          if (ini>=0 && ini<fin && fin<=userFile.length){
            for(x=ini; x<=fin;x++){
                usuRan.push(userFile[x]);
              }
          }
          else {
            res.send({"msg":"Parametros fuera de rango"});
          }
          let respuesta = (usuRan != undefined)? usuRan : {"msg":"no Elementos que mostrar"};
          res.send (respuesta);
        }
      );

//EJERCICIO 3 -->

//Listar todos los usuarios con MLAB
app.get(URL_BASE+'musers',user_controller.getUsers);

//Listar todos usuarios + id con MLAB
app.get(URL_BASE+'musers/:id',user_controller.getUsersId);

//Listar todas las cuentas para un usuario id con MLAB
app.get(URL_BASE+'musers/:id/accounts',user_controller.getAccountUserId);

//POST con MLAB: insertar un usuario
app.post(URL_BASE + 'musers',user_controller.postInsertUser);

//PUT actualizar USERS con parámetro 'id' con MLAB
app.put(URL_BASE + 'musers/:id',user_controller.putUpdateUserId);

//PUT2 actualizar USERS con parámetro 'id' con MLAB
app.put(URL_BASE+ 'usersmLab/:id',user_controller.put2UpdateUserId);

//DELETE user with id MLAB
app.delete(URL_BASE + "musers/:id",user_controller.deleteUserId);

//Login MLAB
app.post(URL_BASE + "mlogin",user_controller.postLogin);

//Method POST loggout MLAB
app.post(URL_BASE + "mlogout",user_controller.postLoggout);

//puerto 3000 es por convencion, Haciendo referencia a la variable
app.listen(PORT,function()
  {
         console.log('Api escuchando en puerto 300');
  });
//app.listen(3000);
//console.log('Api escuchando en puerto 300');
