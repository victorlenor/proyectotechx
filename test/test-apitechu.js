//importar framework de prueas unitarias
var mocha = require('mocha');
var chai = require('chai');
var chaiHttp = require('chai-http');

chai.use(chaiHttp);
var should = chai.should();

describe('Primer test Libre',
function(){//Función manejadora de la suite callback
  it('Test de que hay conexión',//test unitario
      function(done){
        chai.request('http://www.google.com')
        .get('/')//Establecemos prueba para el get
        .end(
            function(err,res){//Definicion de aserciones
              console.log('request terminada');
              //console.log(err);
              //console.log(res);
              res.should.have.status(200);
              done();//Terminamos de evaluar las aserciones
            })
      })
  })

  describe('Primer test suite TechU',
  function(){//Función manejadora de la suite callback
    it('Test de todos los usuarios',//test unitario
        function(done){
          chai.request('http://localhost:3000')
          .get('/apitechu/v1/musers/')//Establecemos prueba para el get
          .end(
              function(err,res){//Definicion de aserciones
                res.should.have.status(200);
                res.body.should.be.a('array');
                for (users of res.body)
                {
                  users.should.have.property('first_name')
                  users.should.have.property('last_name')
                }
                done();//Terminamos de evaluar las aserciones
              })
        })
    })
